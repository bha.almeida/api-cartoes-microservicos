package br.com.mastertech.Cartao.repositories;

import br.com.mastertech.Cartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findByNumero(int Numero);
}
