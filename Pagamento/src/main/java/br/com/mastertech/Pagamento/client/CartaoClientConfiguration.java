package br.com.mastertech.Pagamento.client;

import com.netflix.client.ClientException;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder getCartaoClientDecoder(){
        return new CartaoClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new CartaoClientFallback(), RetryableException.class)
                .withFallback(new CartaoClientFallback(), ClientException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
