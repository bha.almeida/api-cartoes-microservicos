package br.com.mastertech.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT, reason = "Não existem pagamentos para este cartão")
public class CartaoSemPagamentosException extends RuntimeException {
}
