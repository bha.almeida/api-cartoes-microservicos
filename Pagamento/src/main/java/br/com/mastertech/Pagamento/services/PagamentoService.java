package br.com.mastertech.Pagamento.services;

import br.com.mastertech.Pagamento.DTOs.CadastrarPagamentoDTO;
import br.com.mastertech.Pagamento.DTOs.PagamentoCadastradoDTO;
import br.com.mastertech.Pagamento.client.Cartao;
import br.com.mastertech.Pagamento.client.CartaoClient;
import br.com.mastertech.Pagamento.exceptions.CartaoInativoException;
import br.com.mastertech.Pagamento.exceptions.CartaoSemPagamentosException;
import br.com.mastertech.Pagamento.models.Pagamento;
import br.com.mastertech.Pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public PagamentoCadastradoDTO criarPagamento(CadastrarPagamentoDTO cadastrarPagamentoDTO){

        Cartao cartao = cartaoClient.getCartaoById(cadastrarPagamentoDTO.getCartao_id());

        if(!cartao.isAtivo()){
            throw new CartaoInativoException();
        }

        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(cadastrarPagamentoDTO.getDescricao());
        pagamento.setValor(cadastrarPagamentoDTO.getValor());
        pagamento.setCartaoId(cartao.getId());
        pagamentoRepository.save(pagamento);

        PagamentoCadastradoDTO pagamentoCadastradoDTO = new PagamentoCadastradoDTO();
        pagamentoCadastradoDTO.setId(pagamento.getId());
        pagamentoCadastradoDTO.setCartao_id(pagamento.getCartaoId());
        pagamentoCadastradoDTO.setDescricao(pagamento.getDescricao());
        pagamentoCadastradoDTO.setValor(pagamento.getValor());

        return pagamentoCadastradoDTO;
    }

    public List<Pagamento> consultarPagamentosCartao(int idCartao){

        Cartao cartao = cartaoClient.getCartaoById(idCartao);
        List<Pagamento> listaPagamentos = pagamentoRepository.findByCartaoId(cartao.getId());

        if(listaPagamentos == null){
            throw new CartaoSemPagamentosException();
        }
        return listaPagamentos;
    }

    public void deletarPagamentosPorCartao(int idCartao){
        List<Pagamento> listaPagamentos = consultarPagamentosCartao(idCartao);

        Cartao cartao = cartaoClient.getCartaoById(idCartao);
        pagamentoRepository.deleteByCartaoId(cartao.getId());
    }
}
