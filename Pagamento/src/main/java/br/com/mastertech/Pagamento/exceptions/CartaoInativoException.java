package br.com.mastertech.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY, reason = "Não é possível pagar com um cartão inativo")
public class CartaoInativoException extends RuntimeException {
}
