package br.com.mastertech.Pagamento.client;

import br.com.mastertech.Pagamento.exceptions.CartaoNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            return new CartaoNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}
